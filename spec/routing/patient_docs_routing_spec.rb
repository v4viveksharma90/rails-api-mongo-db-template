require "rails_helper"

RSpec.describe PatientDocsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/patient_docs").to route_to("patient_docs#index")
    end

    it "routes to #new" do
      expect(:get => "/patient_docs/new").to route_to("patient_docs#new")
    end

    it "routes to #show" do
      expect(:get => "/patient_docs/1").to route_to("patient_docs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/patient_docs/1/edit").to route_to("patient_docs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/patient_docs").to route_to("patient_docs#create")
    end

    it "routes to #update" do
      expect(:put => "/patient_docs/1").to route_to("patient_docs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/patient_docs/1").to route_to("patient_docs#destroy", :id => "1")
    end

  end
end
