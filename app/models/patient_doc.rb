class PatientDoc
  include Mongoid::Document
  include Mongoid::Timestamps
  field :document_path, type: String
  field :record_del_flg, type: Mongoid::Boolean
  belongs_to :patient
    
end
