class Patient
  include Mongoid::Document
  include Mongoid::Timestamps
  field :fname, type: String
  field :lname, type: String
  field :gender, type: String
  field :date_of_birth, type: String
  field :blood_group, type: String
  field :home_adress, type: String
  field :home_street, type: String
  field :home_city, type: String
  field :home_state, type: String
  field :pin_code, type: Integer
  field :mobile_no, type: String
  field :landline_no, type: String
  field :email, type: String
  field :password, type: String
  field :record_del_flg, type: Mongoid::Boolean
  has_many :appointment
  has_many :patient_doc
  
end
