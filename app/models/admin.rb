class Admin
  include Mongoid::Document
  include Mongoid::Timestamps
  field :fname, type: String
  field :lname, type: String
  field :email, type: String
  field :password, type: String
  field :mobile, type: String
  field :address, type: String
  field :pincode, type: Integer
  field :role, type: String
  field :record_del_flg, type: Mongoid::Boolean
  
end
