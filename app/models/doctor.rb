class Doctor
  include Mongoid::Document
  include Mongoid::Timestamps
  field :fname, type: String
  field :lname, type: String
  field :gender, type: String
  field :qualification, type: String
  field :speciality, type: String
  field :experience, type: String
  field :mobile_no, type: String
  field :email, type: String
  field :password, type: String
  field :work_adress, type: String
  field :work_street, type: String
  field :work_city, type: String
  field :work_state, type: String
  field :work_pin_code, type: Integer
  field :work_landline_no, type: String
  field :home_adress, type: String
  field :home_street, type: String
  field :home_city, type: String
  field :home_state, type: String
  field :doctor_approved_flg, type: Mongoid::Boolean
  field :record_del_flg, type: Mongoid::Boolean
  has_many :appointment
end
