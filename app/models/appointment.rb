class Appointment
  include Mongoid::Document
  include Mongoid::Timestamps
  #field :doctor, type: String
  field :appointment_status_doctor, type: Integer
  field :request_type, type: String
  field :request_description, type: String
  field :appointment_adress_1, type: String
  field :appointment_adress_2, type: String
  field :pincode, type: Integer
  field :checkup_type, type: String
  field :doctor_gender_preference, type: String
  field :appointment_status_customer, type: Integer
  field :number_of_times_request_declined, type: Integer
  field :record_del_flg, type: Mongoid::Boolean
  #embedded_in :patient
  belongs_to :patient
  belongs_to :doctor
  
end
