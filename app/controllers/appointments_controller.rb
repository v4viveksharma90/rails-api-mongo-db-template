class AppointmentsController < ApplicationController
  # GET /appointments
  # GET /appointments.json
  def index
    @appointments = Appointment.all

    render json: @appointments
  end

  # GET /appointments/1
  # GET /appointments/1.json
  def show
    @appointment = Appointment.find(params[:id])

    render json: @appointment
  end

  # POST /appointments
  # POST /appointments.json
  def create
    #@appointment = Appointment.new(params[:appointment])
      
      @appointment = Appointment.new(:appointment_status_doctor=>params[:appointment_status_doctor], :request_type=>params[:request_type], :request_description=>params[:request_description], :appointment_adress_1=>params[:appointment_adress_1], :appointment_adress_2=>params[:appointment_adress_2], :pincode=>params[:pincode], :checkup_type=>params[:checkup_type], :doctor_gender_preference=>params[:doctor_gender_preference], :appointment_status_customer=>params[:appointment_status_customer], :number_of_times_request_declined=>params[:number_of_times_request_declined], :record_del_flg=>params[:record_del_flg], doctor: Doctor.find(params[:doctor]), patient: Patient.find(params[:patient]) )

    if @appointment.save
      render json: @appointment, status: :created, location: @appointment
    else
      render json: @appointment.errors, status: :unprocessable_entity
    end
  end
    
    
  def appointdoc
      @appoint=Appointment.where(doctor_id: params[:doctor_id])
      render json: @appoint
  end

  def appointpatient
      @appoint=Appointment.where(patient_id: params[:patient_id])
      render json: @appoint
  end

# POST appointment/appointment_response_doctor/1
# POST appointment/appointment_response_doctor/1
 def appointment_response_doctor
    @appointment = Appointment.find(params[:id])
    if @appointment.update_attributes(:appointment_status_doctor=>params[:appointment_status_doctor])
      #head :no_content
      render json:@appointment
    else
      render json: @appointment.errors, status: :unprocessable_entity
    end   
  end
    
  # PATCH/PUT /appointments/1
  # PATCH/PUT /appointments/1.json
  def update
    @appointment = Appointment.find(params[:id])
          
    if @appointment.update_attributes(:appointment_status_doctor=>params[:appointment_status_doctor], :request_type=>params[:request_type], :request_description=>params[:request_description], :appointment_adress_1=>params[:appointment_adress_1], :appointment_adress_2=>params[:appointment_adress_2], :pincode=>params[:pincode], :checkup_type=>params[:checkup_type], :doctor_gender_preference=>params[:doctor_gender_preference], :appointment_status_customer=>params[:appointment_status_customer], :number_of_times_request_declined=>params[:number_of_times_request_declined], :record_del_flg=>params[:record_del_flg], doctor: Doctor.find(params[:doctor]), patient: Patient.find(params[:patient]))
      #head :no_content
      render json:@appointment
    else
      render json: @appointment.errors, status: :unprocessable_entity
    end   
  end

  # DELETE /appointments/1
  # DELETE /appointments/1.json
  def destroy
    @appointment = Appointment.find(params[:id])
    @appointment.destroy

    head :no_content
  end
end
