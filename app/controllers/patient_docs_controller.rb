class PatientDocsController < ApplicationController
  # GET /patient_docs
  # GET /patient_docs.json
  def index
    @patient_docs = PatientDoc.all

    render json: @patient_docs
  end

  # GET /patient_docs/1
  # GET /patient_docs/1.json
  def show
    @patient_doc = PatientDoc.find(params[:id])
    render json: @patient_doc
  end

  # POST /patient_docs
  # POST /patient_docs.json
  def create
    @patient_doc = PatientDoc.new(params[:patient_doc])

    if @patient_doc.save
      render json: @patient_doc, status: :created, location: @patient_doc
    else
      render json: @patient_doc.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /patient_docs/1
  # PATCH/PUT /patient_docs/1.json
  def update
    @patient_doc = PatientDoc.find(params[:id])

    if @patient_doc.update(params[:patient_doc])
      head :no_content
    else
      render json: @patient_doc.errors, status: :unprocessable_entity
    end
  end

  # DELETE /patient_docs/1
  # DELETE /patient_docs/1.json
  def destroy
    @patient_doc = PatientDoc.find(params[:id])
    @patient_doc.destroy

    head :no_content
  end
end
