class AdminsController < ApplicationController
  # GET /admins
  # GET /admins.json
  def index
    @admins = Admin.all

    render json: @admins
  end

  # GET /admins/1
  # GET /admins/1.json
  def show
    @admin = Admin.find(params[:id])

    render json: @admin
  end

  # POST /admins
  # POST /admins.json
  def create
    #@admin = Admin.new(params[:admin])
    @admin = Admin.new(:fname=>params[:fname], :lname=>params[:lname], :email=>params[:email], :password=>params[:password], :mobile=>params[:mobile], :address=>params[:address], :pincode=>params[:pincode], :role=>params[:role], :record_del_flg=>params[:record_del_flg])
    if @admin.save
      render json: @admin, status: :created, location: @admin
    else
      render json: @admin.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /admins/1
  # PATCH/PUT /admins/1.json
  def update
    @admin = Admin.find(params[:id])

    #if @admin.update(params[:admin])
    if @admin.update_attributes(:fname=>params[:fname], :lname=>params[:lname], :email=>params[:email], :password=>params[:password], :mobile=>params[:mobile], :address=>params[:address], :pincode=>params[:pincode], :role=>params[:role], :record_del_flg=>params[:record_del_flg])
      head :no_content
    else
      render json: @admin.errors, status: :unprocessable_entity
    end
  end

  # DELETE /admins/1
  # DELETE /admins/1.json
  def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy

    head :no_content
  end
end
