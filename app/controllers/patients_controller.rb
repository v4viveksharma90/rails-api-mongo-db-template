class PatientsController < ApplicationController
  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.all

    render json: @patients
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @patient = Patient.find(params[:id])

    render json: @patient
  end

  # POST /patients
  # POST /patients.json
  def create
    #@patient = Patient.new(params[:patient])
    @patient = Patient.new(:fname=>params[:fname], :lname=>params[:lname], :gender=>params[:gender], :date_of_birth=>params[:date_of_birth], :blood_group=>params[:blood_group], :home_adress=>params[:home_adress], :home_street=>params[:home_street], :home_city=>params[:home_city], :home_state=>params[:home_state], :pin_code=>params[:pin_code], :mobile_no=>params[:mobile_no], :landline_no=>params[:landline_no], :email=>params[:email], :password=>params[:password], :record_del_flg=>params[:record_del_flg])

    if @patient.save
      render json: @patient, status: :created, location: @patient
    else
      render json: @patient.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    @patient = Patient.find(params[:id])

    #if @patient.update(params[:patient])
    if @patient.update_attributes(:fname=>params[:fname], :lname=>params[:lname], :gender=>params[:gender], :date_of_birth=>params[:date_of_birth], :blood_group=>params[:blood_group], :home_adress=>params[:home_adress], :home_street=>params[:home_street], :home_city=>params[:home_city], :home_state=>params[:home_state], :pin_code=>params[:pin_code], :mobile_no=>params[:mobile_no], :landline_no=>params[:landline_no], :email=>params[:email], :password=>params[:password], :record_del_flg=>params[:record_del_flg])
      head :no_content
    else
      render json: @patient.errors, status: :unprocessable_entity
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient = Patient.find(params[:id])
    @patient.destroy

    head :no_content
  end
end
