class DoctorsController < ApplicationController
  # GET /doctors
  # GET /doctors.json
  def index
    @doctors = Doctor.all

    render json: @doctors
  end

  # GET /doctors/1
  # GET /doctors/1.json
  def show
    @doctor = Doctor.find(params[:id])

    render json: @doctor
  end

  # POST /doctors
  # POST /doctors.json
  def create
      
    #@doctor = Doctor.new(params[:doctor])
    @doctor = Doctor.new(:fname=>params[:fname], :lname=>params[:lname], :gender=>params[:gender], :qualification=>params[:qualification], :speciality=>params[:speciality], :experience=>params[:experience], :mobile_no=>params[:mobile_no], :email=>params[:email], :password=>params[:password], :work_adress=>params[:work_adress], :work_street=>params[:work_street], :work_city=>params[:work_city], :work_state=>params[:work_state], :work_pin_code=>params[:work_pin_code], :work_landline_no=>params[:work_landline_no], :home_adress=>params[:home_adress], :home_street=>params[:home_street], :home_city=>params[:home_city], :home_state=>params[:home_state], :doctor_approved_flg=>params[:doctor_approved_flg], :record_del_flg=>params[:record_del_flg])

    if @doctor.save
      render json: @doctor, status: :created, location: @doctor
    else
      render json: @doctor.errors, status: :unprocessable_entity
    end
  end
    

  def changepassword
      @doctor = Doctor.find_by(id: params[:doctor_id], password: params[:password])
      if @doctor.update_attributes(:password => params[:newpass])
        render json: @doctor
      else
         render json: @doctor.errors, status: :unprocessable_entity
      end
  end
    

  # PATCH/PUT /doctors/1
  # PATCH/PUT /doctors/1.json
  def update
    
    @doctor = Doctor.find(params[:id])

    #if @doctor.update(params[:doctor])
    if @doctor.update_attributes(:fname=>params[:fname], :lname=>params[:lname], :gender=>params[:gender], :qualification=>params[:qualification], :speciality=>params[:speciality], :experience=>params[:experience], :mobile_no=>params[:mobile_no], :email=>params[:email], :password=>params[:password], :work_adress=>params[:work_adress], :work_street=>params[:work_street], :work_city=>params[:work_city], :work_state=>params[:work_state], :work_pin_code=>params[:work_pin_code], :work_landline_no=>params[:work_landline_no], :home_adress=>params[:home_adress], :home_street=>params[:home_street], :home_city=>params[:home_city], :home_state=>params[:home_state], :doctor_approved_flg=>params[:doctor_approved_flg], :record_del_flg=>params[:record_del_flg])
      head :no_content
    else
      render json: @doctor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /doctors/1
  # DELETE /doctors/1.json
  def destroy
    @doctor = Doctor.find(params[:id])
    @doctor.destroy

    head :no_content
  end
end
